#pragma once

#include <string>

class Model {
 public:
  virtual Model* Yes() const = 0;
  virtual Model* No() const = 0;
  virtual const std::string& Description() const = 0;

  virtual void AddNode(const std::string& animal, const std::string& trait) = 0;
};
