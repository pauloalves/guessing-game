#include "gui_view.h"

#include <wx/wx.h>

bool GuiView::YesNo(const std::string& msg) {
  wxMessageDialog dialog(nullptr, msg, "Guessing Game",
                         wxCENTER | wxNO_DEFAULT | wxYES_NO);
  return dialog.ShowModal() == wxID_YES;
}

void GuiView::Ok(const std::string& msg) {
  wxMessageDialog dialog(nullptr, msg, "Guessing Game", wxCENTER | wxOK);
  dialog.ShowModal();
}

bool GuiView::OkCancel(const std::string& msg) {
  wxMessageDialog dialog(nullptr, msg, "Guessing Game",
                         wxCENTER | wxOK | wxCANCEL);
  return dialog.ShowModal() == wxID_OK;
}

std::string GuiView::Input(const std::string& msg) {
  wxString value = wxGetTextFromUser(msg, "Guessing Game");
  return value.ToStdString();
}
