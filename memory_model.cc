#include "memory_model.h"

MemoryModel::MemoryModel() : description_("monkey") {
  AddNode("shark", "lives in water");
}

MemoryModel::MemoryModel(const std::string& description)
    : description_(description) {}

MemoryModel::MemoryModel(const std::string& description, MemoryModel* yes,
                         MemoryModel* no)
    : description_(description), yes_(yes), no_(no) {}

MemoryModel::~MemoryModel() {
  delete yes_;
  delete no_;
}

void MemoryModel::AddNode(const std::string& animal, const std::string& trait) {
  no_ = new MemoryModel{std::exchange(description_, trait)};
  yes_ = new MemoryModel{animal};
}
