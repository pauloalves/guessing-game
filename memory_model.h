#pragma once

#include "model.h"

class MemoryModel : public Model {
 public:
  MemoryModel();
  MemoryModel(const std::string& description);
  ~MemoryModel();

  Model* Yes() const { return yes_; }
  Model* No() const { return no_; }
  const std::string& Description() const { return description_; }

  void AddNode(const std::string& animal, const std::string& trait);

 private:
  MemoryModel(const std::string& description, MemoryModel* yes, MemoryModel* no);

  MemoryModel* no_ = nullptr;
  MemoryModel* yes_ = nullptr;
  std::string description_;
};
