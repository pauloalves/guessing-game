#pragma once

#include <wx/app.h>

class App : public wxApp {
 public:
  virtual bool OnInit() wxOVERRIDE;
};
