#pragma once

#include "view.h"
#include "model.h"

class Presenter {
 public:
  Presenter(View* view, Model* model);
  ~Presenter();

  void StartGame();

 private:
  void StartMatch();
  void AddAnimal(Model* n);

  View* view_;
  Model* model_;
};
