#include "app.h"

#include <wx/wx.h>
#include <wx/msgdlg.h>
#include <wx/taskbar.h>

#include "gui_view.h"
#include "memory_model.h"
#include "presenter.h"

wxIMPLEMENT_APP(App);

bool App::OnInit() {
  if (!wxApp::OnInit()) return false;

  GuiView view;
  MemoryModel model;
  Presenter game(&view, &model);
  game.StartGame();

  return false;
}
