#include "console_view.h"

#include <iostream>

bool ConsoleView::YesNo(const std::string& msg) {
  while (true) {
    std::cout << msg;
    std::string option;
    getline(std::cin, option);
    if (option == "y") {
      return true;
    } else if (option == "n") {
      return false;
    }
  }
}

void ConsoleView::Ok(const std::string& msg) {
  std::cout << msg << std::endl;
}

bool ConsoleView::OkCancel(const std::string& msg) {
  std::cout << msg << " (ENTER empty to continue, anything else to cancel)"
            << std::endl;
  std::string option;
  getline(std::cin, option);
  return option.empty();
}

std::string ConsoleView::Input(const std::string& msg) {
  std::cout << msg;
  std::string value;
  getline(std::cin, value);
  return value;
}
