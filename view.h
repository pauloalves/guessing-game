#pragma once

#include <string>

class View {
 public:
  virtual bool YesNo(const std::string& msg) = 0;
  virtual bool OkCancel(const std::string& msg) = 0;
  virtual void Ok(const std::string& msg) = 0;

  virtual std::string Input(const std::string& msg) = 0;
};
