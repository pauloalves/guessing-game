#pragma once

#include "view.h"

class GuiView : public View {
 public:
  bool YesNo(const std::string& msg);
  bool OkCancel(const std::string& msg);
  void Ok(const std::string& msg);

  std::string Input(const std::string& msg);
};
