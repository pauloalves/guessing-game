#include <iostream>
#include <string>

#include "console_view.h"
#include "memory_model.h"
#include "presenter.h"

int main(int argc, char const* argv[]) {
  ConsoleView view;
  MemoryModel model;
  Presenter game(&view, &model);
  game.StartGame();

  return 0;
}
