#include "presenter.h"

Presenter::Presenter(View* view, Model* model) : view_(view), model_(model) {}

Presenter::~Presenter() {}

void Presenter::StartGame() {
  while (view_->OkCancel("Think about an animal...")) {
    StartMatch();
  }
}

void Presenter::StartMatch() {
  Model* curr = model_;
  while (curr) {
    std::string msg = curr->Yes() ? "Does the animal that you thought about "
                                  : "Is the animal that you thought about ";
    msg += curr->Description() + "? (y/n) ";
    if (view_->YesNo(msg)) {
      curr = curr->Yes();
    } else if (curr->No()) {
      curr = curr->No();
    } else {
      AddAnimal(curr);
      return;
    }
  }
  view_->Ok("I win again!");
}

void Presenter::AddAnimal(Model* model) {
  std::string animal =
      view_->Input("What was the animal that you thought about? ");
  std::string trait = view_->Input(
      "A " + animal + " _____ but " + model->Description() +
      " does not (Fill with an animal trait, like 'lives in water') ");

  model->AddNode(animal, trait);
}
